package com.foxfarmroad.illbackyouup.adapter.aws.cloudwatch;

import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEvents;
import com.foxfarmroad.firedancer.UnitTest;
import com.google.inject.Guice;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class CloudWatchModuleUnitTest {
    @Inject
    private AmazonCloudWatchEvents amazonCloudWatchEvents;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new CloudWatchModule()).injectMembers(this);
    }
}
