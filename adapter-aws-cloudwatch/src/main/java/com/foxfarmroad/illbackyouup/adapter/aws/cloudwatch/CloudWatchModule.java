package com.foxfarmroad.illbackyouup.adapter.aws.cloudwatch;

import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEvents;
import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEventsClientBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class CloudWatchModule extends AbstractModule {
    @Override
    protected void configure() {

    }

    @Provides
    @Singleton
    AmazonCloudWatchEvents provideAmazonCloudWatchEvents() {
        return AmazonCloudWatchEventsClientBuilder.standard()
                .build();
//        AWSSecurityTokenServiceClientBuilder stsBuilder = AWSSecurityTokenServiceClientBuilder.standard();
//        stsBuilder.withCredentials(new ProfileCredentialsProvider("ill-back-you-up-dev"));
//        stsBuilder.setEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
//                "sts.us-west-2.amazonaws.com", "us-west-2"));
//        AWSSecurityTokenService sts = stsBuilder.build();
//        GetSessionTokenRequest sessionTokenRequest = new GetSessionTokenRequest().withDurationSeconds(900); // Minimum
//        GetSessionTokenResult sessionTokenResult =
//                sts.getSessionToken(sessionTokenRequest);
//        Credentials credentials = sessionTokenResult.getCredentials();
//        BasicSessionCredentials sessionCredentials = new BasicSessionCredentials(
//                credentials.getAccessKeyId(),
//                credentials.getSecretAccessKey(),
//                credentials.getSessionToken());
//        return AmazonDynamoDBClientBuilder
//                .standard()
//                .withRegion(Regions.US_WEST_2)
//                .withCredentials(new AWSStaticCredentialsProvider(sessionCredentials))
//                .build();
    }
}
